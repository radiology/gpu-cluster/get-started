# get-started

Getting started with the radiology gpu-cluster. This repository is part of the gpu-cluster documentation found [here](https://gitlab.com/radiology/gpu-cluster/documentation/-/wikis/Tutorials/Getting-Started)

## Tutorial
This tutorial shows how to run a simple "hello world" script with PyTorch on the GPU cluster, and approximates pi using a Monte-Carlo method. Keep in mind you will need to change the path in `slurm_script.sh` to your own virtual environment.

## Contributors
- Arno Van Hilten
- Karin Van Garderen
- Kim Van Wijnen
- Robin Camarasa
- Recently updated by: [Juancito van Leeuwen](mailto:j.c.c.vanleeuwen@erasmusmc.nl)