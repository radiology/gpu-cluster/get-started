import torch

if __name__ == '__main__':
    """
    Approximation of pi using Monte-Carlo method
    """
    # Define the precision
    number_of_points = 1000

    # Simulate points in a square
    x = torch.rand(number_of_points).cuda()
    y = torch.rand(number_of_points).cuda()

    # Analyse simulation
    in_circle = 1.0 * ((x ** 2 + y ** 2) < 1).sum().detach()
    approx_pi = 4 * in_circle / number_of_points

    # Display result
    print(f"Approximation of pi: {approx_pi}")
